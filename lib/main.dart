import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

import 'api_call.dart';

List<CameraDescription> cameras;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Main',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'SKC Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        children: <Widget>[
          PageOne(),
      Container(
          child: Column(
              children: <Widget>[
                Container(
                    color: Colors.green[900],
                    height: 400,
                    child: CameraWidget()
                ),
                Expanded(
                    child: Container(
                        color: Colors.green[400],
                        child: IconButton(icon: Icon(Icons.camera, size: 50)),
                    )
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.stretch
          )
      ),
          PageThree(),
          PageFour(),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


class PageOne extends StatefulWidget {
  @override
  _PageOneState createState() => _PageOneState();
}

class CameraWidget extends StatefulWidget {
  @override
  _CameraState createState() => _CameraState();
}

class PageThree extends StatefulWidget {
  String respuestaPost="Sin Datos";
  @override
  _PageThreeState createState() => _PageThreeState();
}

class PageFour extends StatefulWidget {
  @override
  _PageFourState createState() => _PageFourState();
}

class _PageOneState extends State<PageOne> {
  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(-33.427123, -70.613381),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
                Container(color: Colors.blue[900], height: 400, child: GoogleMap(
                  mapType: MapType.hybrid,
                  initialCameraPosition: _kGooglePlex,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ),
                ),
                Expanded( child: Container(color: Colors.blue[400], child: IconButton(icon: Icon(Icons.add_location, size: 50), onPressed: _goToTheLake)))
              ],
          crossAxisAlignment: CrossAxisAlignment.stretch
      )
    );
  }
}

class _CameraState extends State<CameraWidget> {

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  List<CameraDescription> cameras;
  CameraController controller;
  bool isReady = false;
  bool showCamera = true;
  String imagePath;
  // Inputs
  TextEditingController nameController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController abvController = TextEditingController();

  @override
  void initState() {
    super.initState();
    setupCameras();
  }

  Future<void> setupCameras() async {
    try {
      cameras = await availableCameras();
      controller = new CameraController(cameras[0], ResolutionPreset.medium);
      await controller.initialize();
    } on CameraException catch (_) {
      setState(() {
        isReady = false;
      });
    }
    setState(() {
      isReady = true;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: Center(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Center(
                    child: showCamera
                        ? Container(
                      height: 290,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Center(child: cameraPreviewWidget()),
                      ),
                    )
                        : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          imagePreviewWidget(),
                          editCaptureControlRowWidget(),
                        ]),
                  ),
                  showCamera ? captureControlRowWidget() : Container(),
                  cameraOptionsWidget(),
                  beerInfoInputsWidget()
                ],
              ),
            )));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      showInSnackBar('Camera error ${e}');
    }

    if (mounted) {
      setState(() {});
    }
  }

  Widget cameraOptionsWidget() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          showCamera ? cameraTogglesRowWidget() : Container(),
        ],
      ),
    );
  }

  Widget cameraTogglesRowWidget() {
    final List<Widget> toggles = <Widget>[];

    if (cameras != null) {
      if (cameras.isEmpty) {
        return const Text('No camera found');
      } else {
        for (CameraDescription cameraDescription in cameras) {
          toggles.add(
            SizedBox(
              width: 90.0,
              child: RadioListTile<CameraDescription>(
                title: Icon(getCameraLensIcon(cameraDescription.lensDirection)),
                groupValue: controller?.description,
                value: cameraDescription,
                onChanged: controller != null ? onNewCameraSelected : null,
              ),
            ),
          );
        }
      }
    }

    return Row(children: toggles);
  }

  IconData getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.camera_rear;
      case CameraLensDirection.front:
        return Icons.camera_front;
      case CameraLensDirection.external:
        return Icons.camera;
    }
    throw ArgumentError('Unknown lens direction');
  }

  Widget captureControlRowWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        IconButton(
          icon: const Icon(Icons.camera_alt),
          color: Colors.blue,
          onPressed: controller != null && controller.value.isInitialized
              ? onTakePictureButtonPressed
              : null,
        ),
      ],
    );
  }

  Widget beerInfoInputsWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 3, bottom: 4.0),
          child: TextField(
              controller: nameController,
              onChanged: (v) => nameController.text = v,
              decoration: InputDecoration(
                labelText: 'Name the beer',
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3, bottom: 4.0),
          child: TextField(
              controller: countryController,
              onChanged: (v) => countryController.text = v,
              decoration: InputDecoration(
                labelText: "Country name",
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3),
          child: TextField(
              controller: abvController,
              onChanged: (v) => abvController.text = v,
              decoration: InputDecoration(
                labelText: 'ABV',
              )),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Builder(
            builder: (context) {
              return RaisedButton(
                onPressed: () => {},
                color: Colors.lightBlue,
                child: Text('Add beer'),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget editCaptureControlRowWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Align(
        alignment: Alignment.topCenter,
        child: IconButton(
          icon: const Icon(Icons.camera_alt),
          color: Colors.blue,
          onPressed: () => setState(() {
            showCamera = true;
          }),
        ),
      ),
    );
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          showCamera = false;
          imagePath = filePath;
        });
      }
    });
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      return null;
    }
    return filePath;
  }

  Widget cameraPreviewWidget() {
    if (!isReady || !controller.value.isInitialized) {
      return Container();
    }
    return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller));
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Widget imagePreviewWidget() {
    return Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Align(
            alignment: Alignment.topCenter,
            child: imagePath == null
                ? null
                : SizedBox(
              child: Image.file(File(imagePath)),
              height: 290.0,
            ),
          ),
        ));
  }
}

class _PageThreeState extends State<PageThree> {

  Future<void> _getPost() async {
    Post post = await fetchPost();
    setState(() {
    widget.respuestaPost = post.title;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
            children: <Widget>[
              Container(
                  color: Colors.red[900],
                  height: 400,
                  child: Center(child:Text(
                      widget.respuestaPost,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20.0,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w600,
                      )
                  ))
              ),
              Expanded( child: Container(color: Colors.red[400], child: IconButton(icon: Icon(Icons.signal_wifi_4_bar, size: 50), onPressed: _getPost)))
            ],
            crossAxisAlignment: CrossAxisAlignment.stretch
        )
    );
  }
}

class _PageFourState extends State<PageFour> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
            children: <Widget>[
              Container(
                  color: Colors.blueGrey[900],
                  height: 400,
                  child: Tires()
              ),
              Expanded( child: Container(color: Colors.blueGrey[400], child: IconButton(icon: Icon(Icons.airport_shuttle, size: 50), onPressed: null)))
            ],
            crossAxisAlignment: CrossAxisAlignment.stretch
        )
    );
  }
}

class Tires extends StatefulWidget {
  bool enabledTire1 = false;
  bool enabledTire2 = false;
  bool enabledTire3 = false;
  bool enabledTire4 = false;
  Color currentColorTire1 = Color(0xFF909090);
  Color currentColorTire2 = Color(0xFF909090);
  Color currentColorTire3 = Color(0xFF909090);
  Color currentColorTire4 = Color(0xFF909090);
  @override
  _Tires createState() => _Tires();
}

class _Tires extends State<Tires> {
  static final Color Disabled = Color(0xFF909090);
  static final Color Enabled = Colors.red;



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
            Container(padding: EdgeInsets.all(10.0),
                child: Row(
              children: <Widget>[
                buttonTire1(),
                axis,
                buttonTire2()
              ],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            )),
              Container(padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  buttonTire3(),
                  axis,
                  buttonTire4()
                ],
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              )),
          ]
        ),
        alignment: Alignment.center
    );
  }



   void _changeTireColorTire1(){
    setState(() {
      if(widget.enabledTire1) {
        widget.currentColorTire1 = Disabled;
        widget.enabledTire1 = false;
      }
      else {
        widget.currentColorTire1 = Enabled;
        widget.enabledTire1 = true;
      }
    });
  }

  void _changeTireColorTire2(){
    setState(() {
      if(widget.enabledTire2) {
        widget.currentColorTire2 = Disabled;
        widget.enabledTire2 = false;
      }
      else {
        widget.currentColorTire2 = Enabled;
        widget.enabledTire2 = true;
      }
    });
  }

  void _changeTireColorTire3(){
    setState(() {
      if(widget.enabledTire3) {
        widget.currentColorTire3 = Disabled;
        widget.enabledTire3 = false;
      }
      else {
        widget.currentColorTire3 = Enabled;
        widget.enabledTire3 = true;
      }
    });
  }

  void _changeTireColorTire4(){
    setState(() {
      if(widget.enabledTire4) {
        widget.currentColorTire4 = Disabled;
        widget.enabledTire4 = false;
      }
      else {
        widget.currentColorTire4 = Enabled;
        widget.enabledTire4 = true;
      }
    });
  }

  Widget buttonTire1(){return InkWell(
    onTap: _changeTireColorTire1,
      child: new Container(
          height: 100.0,
          width: 50.0,

          decoration: new BoxDecoration(
              boxShadow: [
                new BoxShadow(
                    color: Color(0xFF909090),
                    offset: new Offset(0.0, 0.0),
                    blurRadius: 20.0
                )
              ],
              borderRadius: BorderRadius.circular(30.0),
              color: widget.currentColorTire1
          ),

          child: new Center(
            child: new Text(
                "Rueda",
                style: const TextStyle(
                    fontSize: 12.0,
                    color: Color(0xFF000000),
                    fontWeight: FontWeight.w600
                )
            ),
          )
      ),
  );}

  Widget buttonTire2(){return InkWell(
    onTap: _changeTireColorTire2,
    child: new Container(
        height: 100.0,
        width: 50.0,

        decoration: new BoxDecoration(
            boxShadow: [
              new BoxShadow(
                  color: Color(0xFF909090),
                  offset: new Offset(0.0, 0.0),
                  blurRadius: 20.0
              )
            ],
            borderRadius: BorderRadius.circular(30.0),
            color: widget.currentColorTire2
        ),

        child: new Center(
          child: new Text(
              "Rueda",
              style: const TextStyle(
                  fontSize: 12.0,
                  color: Color(0xFF000000),
                  fontWeight: FontWeight.w600
              )
          ),
        )
    ),
  );}

  Widget buttonTire3(){return InkWell(
    onTap: _changeTireColorTire3,
    child: new Container(
        height: 100.0,
        width: 50.0,

        decoration: new BoxDecoration(
            boxShadow: [
              new BoxShadow(
                  color: Color(0xFF909090),
                  offset: new Offset(0.0, 0.0),
                  blurRadius: 20.0
              )
            ],
            borderRadius: BorderRadius.circular(30.0),
            color: widget.currentColorTire3
        ),

        child: new Center(
          child: new Text(
              "Rueda",
              style: const TextStyle(
                  fontSize: 12.0,
                  color: Color(0xFF000000),
                  fontWeight: FontWeight.w600
              )
          ),
        )
    ),
  );}

  Widget buttonTire4(){return InkWell(
    onTap: _changeTireColorTire4,
    child: new Container(
        height: 100.0,
        width: 50.0,

        decoration: new BoxDecoration(
            boxShadow: [
              new BoxShadow(
                  color: Color(0xFF909090),
                  offset: new Offset(0.0, 0.0),
                  blurRadius: 20.0
              )
            ],
            borderRadius: BorderRadius.circular(30.0),
            color: widget.currentColorTire4
        ),

        child: new Center(
          child: new Text(
              "Rueda",
              style: const TextStyle(
                  fontSize: 12.0,
                  color: Color(0xFF000000),
                  fontWeight: FontWeight.w600
              )
          ),
        )
    ),
  );}

  var axis = new InkWell(
    child: new Container(
        height: 14.0,
        width: 150.0,

        decoration: new BoxDecoration(
            boxShadow: [
              new BoxShadow(
                  color: Color(0xFF909090),
                  offset: new Offset(0.0, 0.0),
                  blurRadius: 20.0
              )
            ],
            borderRadius: BorderRadius.circular(30.0),
            color: Color(0xFFFFFFFF)
        ),

        child: new Center(
          child: new Text(
              "Eje",
              style: const TextStyle(
                  fontSize: 12.0,
                  color: Color(0xFF000000),
                  fontWeight: FontWeight.w600
              )
          ),
        )
    ),
  );
}
